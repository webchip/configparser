package configparser

import (
	"flag"
	"fmt"
	"os"
	"reflect"
	"strings"
	"time"
	"unsafe"
)

func Do(s interface{}) error {
	valOf := reflect.ValueOf(s).Elem()
	typeOf := valOf.Type()

	for i := 0; i < typeOf.NumField(); i++ {
		field := typeOf.Field(i)
		valField := valOf.FieldByIndex(field.Index)

		if cfgName, ok := field.Tag.Lookup("cfg-name"); ok {
			switch valField.Kind() {
			case reflect.String:
				flag.StringVar((*string)(unsafe.Pointer(valField.UnsafeAddr())), cfgName, valField.String(), "")
			case reflect.Bool:
				flag.BoolVar((*bool)(unsafe.Pointer(valField.UnsafeAddr())), cfgName, valField.Bool(), "")
			case reflect.Int:
				flag.IntVar((*int)(unsafe.Pointer(valField.UnsafeAddr())), cfgName, int(valField.Int()), "")
			case reflect.Int64:
				durationType := reflect.TypeOf(time.Nanosecond)
				if valField.Type() == durationType {
					flag.DurationVar((*time.Duration)(unsafe.Pointer(valField.UnsafeAddr())), cfgName, time.Duration(valField.Int()), "")
				} else {
					flag.Int64Var((*int64)(unsafe.Pointer(valField.UnsafeAddr())), cfgName, valField.Int(), "")
				}
			case reflect.Uint:
				flag.UintVar((*uint)(unsafe.Pointer(valField.UnsafeAddr())), cfgName, uint(valField.Uint()), "")
			case reflect.Uint64:
				flag.Uint64Var((*uint64)(unsafe.Pointer(valField.UnsafeAddr())), cfgName, valField.Uint(), "")
			case reflect.Float64:
				flag.Float64Var((*float64)(unsafe.Pointer(valField.UnsafeAddr())), cfgName, valField.Float(), "")
			}
		}
	}

	var envErr error
	flag.VisitAll(func(i *flag.Flag) {
		envName := strings.ToUpper(i.Name)
		if envVar := os.Getenv(envName); envVar != "" {
			if err := i.Value.Set(envVar); err != nil {
				envErr = fmt.Errorf("%w: env %s=%s", err, envName, envVar)
			}
		}
	})
	if envErr != nil {
		return envErr
	}

	flag.Parse()

	return nil
}
